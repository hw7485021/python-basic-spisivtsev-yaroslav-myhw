# Завдання 1 Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові.
# Наприклад "The [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'".
# Слово та номер символу отримайте за допомогою input() або скористайтеся константою. Наприклад (слово - "Python"
# а номер символу 3) - "The 3 symbol in 'Python' is 't' ".
print("Завдання 1\n"
      "---------------------------")

word = input("Write a word --> ")
character = input("Type the number of the character --> ")

while character.isalpha() or int(character) > len(word) or int(character) <= 0:
    if character.isalpha():
        print("Error! Type only numbers!Try again!")
    elif int(character) > len(word):
        print(f'Error! Word "{word}" is less than "{character}" characters! Try again!')
    elif int(character) <= 0:
        print("Error! Number of the character can't be less than 0 ")
    character = input("Type the number of the character --> ")
character = int(character)
print(f'The "{character}" symbol in "{word}" is "{word[character-1]}" ')

# Задача 2 Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою). Напишіть код, який
# визначить кількість слів, в цих даних.
print("Завдання 2\n"
      "---------------------------")
text = input("Type the text --> ")
list_of_word = text.split()
print(f"Number of words is {len(list_of_word)}")

# Завдання 3.Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0,
# 'Lorem Ipsum']. Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float),
# які є в lst1. Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.
print("Завдання 3\n"
      "---------------------------")
lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0,'Lorem Ipsum','3.8',4.5 ,4,'.'] # Додали також значення
# типу float та значення типу float завернуте у String для перевірки"
result_list = []
for element in lst1:
    if type(element) == str:
        if element.isdigit():
            result_list.append(int(element))
        elif element.find("."):
            try:
                result_list.append(float(element))
            except ValueError:
                continue
    elif type(element) == int:
        result_list.append(element)
    elif type(element) == float:
        result_list.append(element)

print(result_list)
