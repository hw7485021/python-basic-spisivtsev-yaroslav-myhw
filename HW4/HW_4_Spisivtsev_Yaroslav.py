# Завдання 1.  Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві
# голосні літери підряд.

print("Завдання 1\n"
      "---------------------------")
string = "book apple train dog good service lemon"
string_list = string.split()
word_counter = 0
vowels = "aeiouAEIOU"

for word in string_list:
    for char in range(len(word)-1):
        if word[char] in vowels and word[char+1] in vowels:
            word_counter += 1
            break

print(f"The number of words with two consecutive vowels is {word_counter}")

# Завдання 2. Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів
# і цінами: { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324,
# "x-store":37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}. Напишіть код, який знайде і виведе
# на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"
print("Завдання 2\n"
      "---------------------------")
lower_limit = 35.9
upper_limit = 37.339
dict_of_prices = {"cito": 47.999,
                  "BB_studio": 42.999,
                  "momo": 49.999,
                  "main-service": 37.245,
                  "buy.now": 38.324,
                  "x-store": 37.166,
                  "the_partner": 38.988,
                  "store": 37.720,
                  "rozetka": 38.003}
match = ''
for key, value in dict_of_prices.items():
    if lower_limit <= value <= upper_limit:
        match += ''.join('"' + key + '", ')

print(f"> match: {match[:-2]}")