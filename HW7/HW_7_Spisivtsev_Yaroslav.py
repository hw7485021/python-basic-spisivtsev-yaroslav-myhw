# #Напишіть гру "Rock paper scissors" https://en.wikipedia.org/wiki/Rock_paper_scissors
# Грають гравець і компʼютер. Вdід даних гравцем - через input()
# Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг.
# Не забувайте що кожна функція має виконувати тільки одне завдання і про правила написання коду (DRY, KISS, YAGNI).

import random
def get_computer_choice():
    """
    The function determines the computer's choice
    :return computer_choice
    """
    computer_choice = random.choice(['r','p','s'])
    return  computer_choice

def get_user_choice():
    """
    The function takes the user's choice
    :return user_choice
    """
    user_choice = ''
    while user_choice not in ['r','p','s'] :
        user_choice = input("Your choose is? (rock(r), paper(p), scissors(s)):  ").lower()
        if user_choice in ['r', 'p', 's']:
            return user_choice
        print('Incorrect choose. You can choose only r,p,s. Try again!')

def who_is_win(computer_choice,user_choice):
    """
    The rules for determining the winner are specified in this function
    :return winer.
    """
    if computer_choice == user_choice:
        return ('Tie!')
    if ((computer_choice =='r' and user_choice == 'p') or
        (computer_choice == 'p' and user_choice == 's') or
        (computer_choice == 's' and user_choice == 'r')):
        return "user"
    else:
        return "computer"

def game ():
    """
    The function determines the winner. It describes the winner and asks the user if he wants to play again.
    """
    get = {'r':'rock','p':"paper",'s':'scissors'}
    while True:
        print('The Game:"ROCK, PAPER, SCISSORS"\n'
              '*------------------------------*')
        user_choice = get_user_choice()
        print(f'User chose "{get[user_choice]}"')
        computer_choice = get_computer_choice()
        print(f'Computer chose "{get[computer_choice]}"')
        winner = who_is_win(computer_choice,user_choice)

        if winner == "user":
            print('________________________________\n'
                  '|||         Win user!        |||\n'
                  '--------------------------------')
        elif winner == "computer":
            print('________________________________\n'
                  '|||       Win computer!      |||\n'
                  '--------------------------------')
        else: print('________________________________\n'
                    '|||           Draw!          |||\n'
                    '--------------------------------')

        play_again = ""

        while True:
            play_again =input("Would you like to play again? (y/n): ").lower()
            if play_again == 'y' or play_again == 'n': break
            else: print('Incorrect choose')
        if play_again == 'n': break

game()
