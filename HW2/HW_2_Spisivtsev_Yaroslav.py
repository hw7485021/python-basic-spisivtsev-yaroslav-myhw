age = input("Введіть свій вік: -->")

if not age.isdigit():
    print("Введений вік некоректний. Будь ласка, введіть число.")
else:
    age = int(age)
    if "7" in str(age):
        print("Вам пощастить!")
    elif age < 6:
        print("Де твої батьки?")
    elif age < 16:
        print("Це фільм для дорослих!")
    elif age > 65:
        print("Покажіть пенсійне посвідчення!")
    else:
        print("А білетів вже немає!")


