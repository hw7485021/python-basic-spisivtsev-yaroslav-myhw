#Завдання: Доопрацюйте гру з заняття наступним чином:
#
# 1.+Додайте вибір слова з будь-якого джерела на ваш вибір
# 2.+Додайте лічильник спроб вгадати слово і вихід з циклу по переповненню лічильника
# 3.+Додайте більш інформативні повідомлення (список можливих букв для введення, повідомлення про невірне введення,
#   повідомлення про кількість спроб що залишилися і тд)
# 4.+ Опційно. Додайте можливість повторювати гру. Запитати в гравця чи хоче він повторити гру Yes/No і повторювати якщо
#   він введе Yes, завершити якщо введе No

import random
# todo: fix it, strip, lowercase (normalize data here)

# hardcode - list of words - random
list_of_words = "Elephant,Butterfly,Giraffe,Alligator,Telephone,Computer,Television,Refrigerator,Watermelon," \
                "Pineapple,Backpack,Scissors,Umbrella,Guitar,Piano,Kangaroo,Dragonfly,Strawberry,Octopus," \
                "Rainbow,Cucumber,Crocodile,Helicopter,Waterfall,Sandwich,Motorcycle,Library,Telescope," \
                "Basketball,Hamburger"

available_letters = 'abcdefghijklmnopqrstyuwxvz'
# add possible to replay the game
while True:
    word = random.choice(list_of_words.split(",")).lower()
    guess_result = '?' * len(word)
    guessed_letters = []
    entered_letters = set()

    # add attempt counter
    counter_of_tries = len(word)

    while guess_result != word:

        print(f'Guess the word: {guess_result}')

        while True:
            if counter_of_tries <= 0:
                break
            user_letter = input('Enter the letter: ').lower()
            if len(user_letter) != 1:
                print('Too long or too short!')
                continue
            elif user_letter not in available_letters:
                print('Unavailable!')
                continue
            elif user_letter in entered_letters:
                counter_of_tries -= 1
                if counter_of_tries > 0:
                    print(f'Duplicate!You have {counter_of_tries} tries! Try again!')
                    continue
                else:
                    print(f'Duplicate!You have {counter_of_tries} tries!. You lose!')
                continue

            entered_letters.add(user_letter)
            break

        if user_letter in word:
            guessed_letters.append(user_letter)
            guess_result = ''.join(['?' if char not in guessed_letters else char for char in word])
        else:
            counter_of_tries -= 1
            if counter_of_tries < 0:
                None
            elif counter_of_tries > 0:
                print(f'Incorrect!You have {counter_of_tries} tries! Try again!')
            else:
                print(f'Incorrect!You have {counter_of_tries} tries! You lose!')

        if guess_result == word:
            print('___________________________\n'
                  '|||   Congratulations!  |||\n'
                  '---------------------------')
        if guess_result == word or counter_of_tries <= 0:
            print(f'Gusessed world is "{word}"')
            break

    while True:
        play_again =input("*************************************\n"
                          "Would you like to play again? (Yes/No): ").lower()
        if play_again == 'yes' or play_again == 'no': break
        else: print('Incorrect choose')
    if play_again == 'no': break

