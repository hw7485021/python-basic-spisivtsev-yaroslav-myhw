# 1. Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від
# "Транспортний засіб". Наповніть класи атрибутами на свій розсуд.
# 2. Cтворіть обʼєкти класів "Автомобіль", "Літак", "Корабель".

class Vehicle:
    def get_info(self):
        return f'This is {self.color} {self.__class__.__name__}. Weight is {self.weight} kg, volume is {self.volume} m3, speed is {self.speed} km/h'

class Car(Vehicle):
    color = 'red'
    weight = 1000
    volume = 5
    speed = 120
    can = 'drive'

class Airplane(Vehicle):
    color = "blue"
    weight = 700
    volume = 6
    speed = 250
    can = 'fly'

class Ship(Vehicle):
    color = "white"
    weight = 3000
    volume = 6
    speed = 250
    can = 'swim'

car = Car()
airplane = Airplane()
ship = Ship()

print(car.get_info())
print(airplane.get_info())
print(ship.get_info())